package com.example.eef.sewquicktool;

        import android.content.Context;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.graphics.Bitmap;
        import android.net.Uri;
        import android.os.Bundle;
        import android.support.v7.app.AppCompatActivity;
        import android.support.v7.widget.Toolbar;
        import android.view.View;
        import android.widget.ArrayAdapter;
        import android.widget.ImageView;
        import android.widget.TextView;
        import android.database.sqlite.SQLiteDatabase;
        import android.database.SQLException;


        import com.bumptech.glide.request.RequestOptions;
        import com.example.eef.sewquicktool.R;


        import java.io.IOException;
        import java.util.ArrayList;
        import java.util.List;

public class ProjectActivity extends AppCompatActivity {

    private MyDBHandler mDBHelper;
    private SQLiteDatabase mDb;
    private Bitmap bitmap;



    private static final String DATABASE_NAME = "project_Database.db";
    MyDBHandler dbHandler;
    TextView myText;
    Linksgirls link;
    ArrayAdapter arrayAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.project_activity);

        Bundle intent = getIntent().getExtras();

        SharedPreferences pref = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String intentgirlboy = pref.getString("name", "");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(intentgirlboy);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                                                 @Override
                                                 public void onClick(View v) {
                                                     ProjectActivity.super.finish();

                                                 }
                                             }
        );



        dbHandler = new MyDBHandler(this);
        dbHandler.openDataBase();
        TextView myText = (TextView) findViewById(R.id.intentInfo);
        Project project = dbHandler.getProjectFromDatabase(getIntent().getStringExtra("projectKey"));
        myText.setText(project.getfabricType());


        TextView myTextC = (TextView) findViewById(R.id.title);
        myTextC.setText(project.getProject());

        getSupportActionBar().setSubtitle(project.getProject());

        final String age = (String) project.getprojectKey() ;
        TextView myTextD = (TextView) findViewById(R.id.titletext);
        if (age != null && age.equals("babytops") || age.equals("babysweaters") || age.equals("babycardigans") || age.equals("babycoats") || age.equals("babytrousers") || age.equals("babydresses") || age.equals("babyskirts") || age.equals("babyonesies") || age.equals("babyswimwear") || age.equals("babyswaddles")) {
            myTextD.setText("Use airy and light fabrics when sewing clothes for babies. The skin is fragile and needs fabric that is gentle on the skin. The recommended fabrics, together with our selected sewing tutorials are mentioned below.");
        } else {
          myTextD.setText("Below, we have selected some great information resources from across the web." +
                  " These tutorials and guides help you select your fabric and get you started sewing your desired project. It also helps you use a sewing machine for the first time and offers some great guides in case you experience troubles with the machine.");
        }


            final List<Linksgirls> listgirl = dbHandler.getgirlFromDatabase(getIntent().getStringExtra("projectKey"));
            final List<Linksgirls> list = dbHandler.getLinksFromDatabase(getIntent().getStringExtra("projectKey"));


            List<ImageView> images = new ArrayList<ImageView>();
            images.add((ImageView) findViewById(R.id.image0));
            images.add((ImageView) findViewById(R.id.image1));
            images.add((ImageView) findViewById(R.id.image2));
            images.add((ImageView) findViewById(R.id.image3));
            images.add((ImageView) findViewById(R.id.image4));
            images.add((ImageView) findViewById(R.id.image5));
            images.add((ImageView) findViewById(R.id.image6));
            images.add((ImageView) findViewById(R.id.image7));
            images.add((ImageView) findViewById(R.id.image8));
            images.add((ImageView) findViewById(R.id.image9));
            images.add((ImageView) findViewById(R.id.image10));
            images.add((ImageView) findViewById(R.id.image11));
            images.add((ImageView) findViewById(R.id.image12));
            images.add((ImageView) findViewById(R.id.image13));
            images.add((ImageView) findViewById(R.id.image14));
            images.add((ImageView) findViewById(R.id.image15));
            images.add((ImageView) findViewById(R.id.image16));


            List<TextView> textImages = new ArrayList<TextView>();

            textImages.add((TextView) findViewById(R.id.imagetext0));
            textImages.add((TextView) findViewById(R.id.imagetext1));
            textImages.add((TextView) findViewById(R.id.imagetext2));
            textImages.add((TextView) findViewById(R.id.imagetext3));
            textImages.add((TextView) findViewById(R.id.imagetext4));
            textImages.add((TextView) findViewById(R.id.imagetext5));
            textImages.add((TextView) findViewById(R.id.imagetext6));
            textImages.add((TextView) findViewById(R.id.imagetext7));
            textImages.add((TextView) findViewById(R.id.imagetext8));
            textImages.add((TextView) findViewById(R.id.imagetext9));
            textImages.add((TextView) findViewById(R.id.imagetext10));
            textImages.add((TextView) findViewById(R.id.imagetext11));
            textImages.add((TextView) findViewById(R.id.imagetext12));
            textImages.add((TextView) findViewById(R.id.imagetext13));
            textImages.add((TextView) findViewById(R.id.imagetext14));
            textImages.add((TextView) findViewById(R.id.imagetext15));
            textImages.add((TextView) findViewById(R.id.imagetext16));


            if (intentgirlboy != null && intentgirlboy.equals("Girls")) {
                for (int i = 0; i < listgirl.size(); i++) {
                    //    MyJTextField.setText(MyJTextField.getText() + MyArrayList.get(i) + "\n");

                    Linksgirls linkItem = listgirl.get(i);


                    //images.get(i).setVisibility(View.VISIBLE);

                    GlideApp
                            .with(this)
                            .setDefaultRequestOptions(new RequestOptions().timeout(30000))
                            .load(linkItem.getimage())
                            .fitCenter()
                            // scale to fill the ImageView
                            .into(images.get(i));


                }


                for (int i = listgirl.size(); i < images.size(); i++) {
                    images.get(i).setVisibility(View.GONE);
                    textImages.get(i).setVisibility(View.GONE);
                }


                for (int a = 0; a < listgirl.size(); a++) {
                    Linksgirls naamItem = listgirl.get(a);
                    textImages.get(a).setText(naamItem.getname());

                }


                for (int j = 0; j < images.size(); j++) {

                    final int finalJ = j;
                    images.get(j).setOnClickListener(new ImageView.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Linksgirls link = listgirl.get(finalJ);

                            Intent browserIntent = new Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse(link.getlink())
                            );
                            startActivity(browserIntent);
                        }
                    });

                }


            } else {

                for (int i = 0; i < list.size(); i++) {
                 
                    Linksgirls linkItem = list.get(i);


                    GlideApp
                            .with(this)
                            .setDefaultRequestOptions(new RequestOptions().timeout(30000))
                            .load(linkItem.getimage())
                            .fitCenter()
                            // scale to fill the ImageView
                            .into(images.get(i));


                }


                for (int i = list.size(); i < images.size(); i++) {
                    images.get(i).setVisibility(View.GONE);
                    textImages.get(i).setVisibility(View.GONE);
                }


              


                for (int a = 0; a < list.size(); a++) {
                    Linksgirls naamItem = list.get(a);
                    textImages.get(a).setText(naamItem.getname());

                }



                for (int j = 0; j < images.size(); j++) {

                    final int finalJ = j;
                    images.get(j).setOnClickListener(new ImageView.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Linksgirls link = list.get(finalJ);

                            Intent browserIntent = new Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse(link.getlink())
                            );
                            startActivity(browserIntent);
                        }
                    });

                }


            }
        dbHandler.close();
        }

    }







