package com.example.eef.sewquicktool;


public class Linksgirls {


    private static final String DATABASE_NAME = "project_Database.db";
    public static final String COLUMN_PROJECT_LINKS_ID = "ID";
    public static final String TABLE_NAME = "linksgirls";
    public static final String COLUMN_NAAM = "name";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_LINK = "link";
    public static final String COLUMN_PROJECTLINKS_PROJECTKEY = "projectKey";
    public static final String COLUMN_IMAGE = "image";
    public static final String COLUMN_BOY = "boy";
    public static final String COLUMN_GIRL = "girl";

    private int id;
    private String name;
    private String type;
    private String link;
    private String projectKey;
    private String image;
    private int boy;
    private int girl;


    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    +  COLUMN_PROJECT_LINKS_ID + "INTEGER, "
                    + COLUMN_NAAM + "TEXT,"
                    + COLUMN_TYPE + "TEXT, "
                    + COLUMN_LINK + "TEXT, "
                    + COLUMN_PROJECTLINKS_PROJECTKEY + "TEXT, "
                    + COLUMN_IMAGE + "TEXT"
                    + COLUMN_BOY + "INTEGER, "
                    + COLUMN_GIRL + "INTEGER, " +
                     ")";

public Linksgirls() {

}


        public Linksgirls( int id, String name, String type, String link, String projectKey, String
        image, int boy, int girl){
            this.id = id;
            this.name = name;
            this.type = type;
            this.link = link;
            this.projectKey = projectKey;
            this.image = image;
            this.boy = boy;
            this.girl = girl;

        }

        public int getId () {     return id; }

        public void setId ( int id){ this.id = id; }

        public String getname () { return name; }

        public String gettype () {
            return type;
        }

        public void setname (String name){
            this.name = name;
        }

        public String getlink () {
            return link;
        }

        public void setimage (String image){
            this.image = image;
        }

        public void settype (String type){
            this.type = type;
        }

        public void setlink (String link){
            this.link = link;
        }

        public String getprojectKey () {
            return projectKey;
        }

        public String getimage () {
            return image;
        }

        public void setprojectKey (String projectKey){
            this.projectKey = projectKey;
        }

        public int getboy () {     return boy; }

        public void setboy ( int boy){ this.boy = boy; }

        public void setgirl ( int girl){ this.girl = girl; }

        public int getgirl () {  return girl; }


    }





