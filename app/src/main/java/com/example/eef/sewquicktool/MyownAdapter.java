package com.example.eef.sewquicktool;

import java.util.HashMap;
import java.util.List;
import android.content.Context;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.eef.sewquicktool.R;

/**
 * Created by Eef on 23-3-2018.
 */

public class MyownAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List <String> listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap <String, List <String>> listHashMap;

    public MyownAdapter(Context context, List<String> listDataHeader, HashMap <String, List<String>> listHashMap) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listHashMap = listHashMap; // hashmap is een mappen interface//
    }

    //hier kijk hoe groot de lijst is- hoeveel headers//
    @Override
    public int getGroupCount() {
        return listDataHeader.size();
    }


    //kijkt hoeveel subitems er zijn//
    @Override
    public int getChildrenCount(int i) {
        return listHashMap.get(listDataHeader.get(i)).size();
    }

    //returnes hoeveelheid dataheaders//
    @Override
    public Object getGroup(int i) {
        return listDataHeader.get(i);
    }


    // return de map met headers en items//
    @Override
    public Object getChild(int i, int i1) {
        return listHashMap.get(listDataHeader.get(i)).get(i1);
    }


    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }


    //hier inflate je dus de layout alleen als view null is//
    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        String headerTitle = (String) getGroup(i);
        if (view == null)
        {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view= inflater.inflate(R.layout.list_group,null);
        }
        TextView lblListHeader = (TextView) view.findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return view; // laat list Header zien//



    }

    // hier inflate je dus alleen de children (de items) als de view null is//
    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        final String childText = (String) getChild(i,i1);
        if (view == null)
        {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view= inflater.inflate(R.layout.list_item,null);
        }
        TextView txtListChild = (TextView) view.findViewById(R.id.lblListItem);
        txtListChild.setText(childText);
        return view;

    }


    @Override
    public boolean isChildSelectable(int i, int i1)
    {
        return true;
    }

}



