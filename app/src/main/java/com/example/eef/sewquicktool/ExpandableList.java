package com.example.eef.sewquicktool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ExpandableListView;

import com.example.eef.sewquicktool.R;


public class ExpandableList extends AppCompatActivity {

    private MyownAdapter listAdapter;
    private ExpandableListView listView;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listHash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expandablelist);

        String intent = getIntent().getStringExtra("sendMessage");


        //sets title to toolbar and makes it a backbutton

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(intent);
        getSupportActionBar().setSubtitle("Projects");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent main = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(main);
            }
        });

        //remember getintent (girl or boy) and pass it along into projectactivity.java (next activity)
        SharedPreferences pref = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);

// editor object to make changes
        SharedPreferences.Editor edit = pref.edit();

// Store data. 
        edit.putString("name", intent);
        edit.commit();




        // get the listview
        listView = (ExpandableListView) findViewById(R.id.lvExp);

        // preparing list data
        initData();


        listAdapter = new MyownAdapter(this, listDataHeader, listHash);
        listView.setAdapter(listAdapter);

        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                if (groupPosition == 0) {
                    if (childPosition == 0) {
                        Intent child0Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "babytops";
                        child0Intent.putExtra("projectKey", sendMessage);
                        startActivity(child0Intent);
                    }
                    if (childPosition == 1) {
                        Intent child1Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "babysweaters";
                        child1Intent.putExtra("projectKey", sendMessage);
                        startActivity(child1Intent);
                    }
                    if (childPosition == 2) {
                        Intent child2Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "babycardigans";
                        child2Intent.putExtra("projectKey", sendMessage);
                        startActivity(child2Intent);
                    }
                    if (childPosition == 3) {
                        Intent child3Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "babycoats";
                        child3Intent.putExtra("projectKey", sendMessage);
                        startActivity(child3Intent);
                    }
                    if (childPosition == 4) {
                        Intent child4Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "babytrousers";
                        child4Intent.putExtra("projectKey", sendMessage);
                        startActivity(child4Intent);
                    }
                    if (childPosition == 5) {
                        Intent child5Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "babydresses";
                        child5Intent.putExtra("projectKey", sendMessage);
                        startActivity(child5Intent);
                    }
                    if (childPosition == 6)  {
                        Intent child6Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "babyskirts";
                        child6Intent.putExtra("projectKey", sendMessage);
                        startActivity(child6Intent);
                    }
                    if (childPosition == 7) {
                        Intent child7Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "babyonesies";
                        child7Intent.putExtra("projectKey", sendMessage);
                        startActivity(child7Intent);
                    }
                    if (childPosition == 8) {
                        Intent child8Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "babyswimwear";
                        child8Intent.putExtra("projectKey", sendMessage);
                        startActivity(child8Intent);
                    }
                    if (childPosition == 9) {
                        Intent child9Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "babyswaddles";
                        child9Intent.putExtra("projectKey", sendMessage);
                        startActivity(child9Intent);
                    }

                }

                if (groupPosition == 1) {
                    if (childPosition == 0) {
                        Intent child0Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "toddlertops";
                        child0Intent.putExtra("projectKey", sendMessage);
                        startActivity(child0Intent);
                    }

                    if (childPosition == 1) {
                        Intent child1Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "toddlersweaters";
                        child1Intent.putExtra("projectKey", sendMessage);
                        startActivity(child1Intent);
                    }
                    if (childPosition == 2) {
                        Intent child2Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "toddlercardigans";
                        child2Intent.putExtra("projectKey", sendMessage);
                        startActivity(child2Intent);
                    }
                    if (childPosition == 3) {
                        Intent child3Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "toddlercoats";
                        child3Intent.putExtra("projectKey", sendMessage);
                        startActivity(child3Intent);
                    }
                    if (childPosition == 4) {
                        Intent child4Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "toddlertrousers";
                        child4Intent.putExtra("projectKey", sendMessage);
                        startActivity(child4Intent);
                    }
                    if (childPosition == 5) {
                        Intent child5Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "toddlerdresses";
                        child5Intent.putExtra("projectKey", sendMessage);
                        startActivity(child5Intent);
                    }
                    if (childPosition == 6) {
                        Intent child6Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "toddlerskirts";
                        child6Intent.putExtra("projectKey", sendMessage);
                        startActivity(child6Intent);
                    }
                    if (childPosition == 7  ) {
                        Intent child7Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "toddleronesies";
                        child7Intent.putExtra("projectKey", sendMessage);
                        startActivity(child7Intent);
                    }
                    if (childPosition == 8) {
                        Intent child8Intent = new Intent(getBaseContext(), ProjectActivity.class);
                        String sendMessage  = "toddlerswimwear";
                        child8Intent.putExtra("projectKey", sendMessage);
                        startActivity(child8Intent);
                    }

                }

                return false;
            }
        });


    }


    private void initData() {


        listDataHeader = new ArrayList<>();
        listHash = new HashMap<>();

        listDataHeader.add("Baby (0-12 Months)");
        listDataHeader.add("Toddler/ Kids");



        List<String> baby = new ArrayList<>();
        baby.add("Tops");
        baby.add("Sweaters");
        baby.add("Cardigans");
        baby.add("Coats");
        baby.add("Trousers");
        baby.add("Dresses");
        baby.add("Skirts");
        baby.add("Onesies");
        baby.add("Swimwear");
        baby.add("Swaddles");






        List<String> toddler = new ArrayList<>();
        toddler.add("Tops");
        toddler.add("Sweaters");
        toddler.add("Cardigans");
        toddler.add("Coats");
        toddler.add("Trousers");
        toddler.add("Dresses");
        toddler.add("Skirts");
        toddler.add("Onesies");
        toddler.add("Swimwear");


        listHash.put(listDataHeader.get(0), baby);
        listHash.put(listDataHeader.get(1), toddler);





    }

    private void setGroupIndicatorToRight() {
        /* Get the screen width */
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;

        listView.setIndicatorBounds(width-getDipsFromPixel(24), width-getDipsFromPixel(5));
    }

    // Convert pixel to dip
    public int getDipsFromPixel(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }


}








