package com.example.eef.sewquicktool;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.SQLException;
import android.util.Log;
import android.widget.ArrayAdapter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;


public class MyDBHandler extends SQLiteOpenHelper {
//naam database en naam tabel ID, project(kledingstuk) en type stof)
    private static final int DATABASE_VERSION = 37;
    private static String DB_PATH = "";
    private static final String DATABASE_NAME = "project_Database.db";
    public static final String COLUMN_PROJECTKEY = "projectKey";
    public static final String TABLE_NAME = "Projects";
    private static final String COLUMN_ID = "ID";
    private static final String COLUMN_PROJECT = "project";
    private static final String COLUMN_FABRICTYPE = "fabricType";


// alles voor de tabel linksgirls waar naam, type en link in staat
    public static final String TABLE_LINKS_NAME = "linksgirls";
    public static final String COLUMN_PROJECT_LINKS_ID = "ID";
    public static final String COLUMN_NAAM = "name";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_LINK = "link";
    public static final String COLUMN_PROJECTLINKS_PROJECTKEY = "projectKey";
    public static final String COLUMN_IMAGE = "image";
    public static final String COLUMN_BOY = "boy";
    public static final String COLUMN_GIRL = "girl";



    private SQLiteDatabase mDataBase;
    private final Context mContext;
    private boolean mNeedUpdate = false;


//maak de tabel als deze niet al gemaakt is
    public static final String tableprojects = "CREATE TABLE IF NOT EXISTS " + "Projects" + "(" + COLUMN_ID + "INTEGER, " + COLUMN_PROJECT + "TEXT," + COLUMN_FABRICTYPE + "TEXT, " + COLUMN_PROJECTKEY + "TEXT" + ")";
    public static final String tablelinks = "CREATE TABLE IF NOT EXISTS " + "linksgirls" + "(" + COLUMN_PROJECT_LINKS_ID + "INTEGER, " + COLUMN_NAAM + "TEXT," + COLUMN_TYPE + "TEXT, " + COLUMN_LINK + "TEXT, " + COLUMN_PROJECTLINKS_PROJECTKEY + "TEXT, " + COLUMN_IMAGE + "TEXT" + COLUMN_BOY + "INTEGER, " + COLUMN_GIRL + "INTEGER" + ")";
    private int boy;
    private int girl;


    //path name van tabel
    public MyDBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        if (android.os.Build.VERSION.SDK_INT >= 17)
            DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        else
            DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        this.mContext = context;

        copyDataBase();

       // this.DB_PATH = "/data/data/" + context.getPackageName() + "/" + "databases/";
       Log.e("path 1", DB_PATH);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // create notes table
        //db.execSQL(Project.CREATE_TABLE);

        db.execSQL(tableprojects);
        db.execSQL(tablelinks);
        Log.e("", "table created");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LINKS_NAME);
        onCreate(db);

        }

    private boolean checkDataBase() {
        File dbFile = new File(DB_PATH + DATABASE_NAME);
        return dbFile.exists();
    }

    private void copyDataBase() {
        if (!checkDataBase()) {
            this.getReadableDatabase();
            this.close();
            try {
                copyDBFile();
            } catch (IOException mIOException) {
                throw new Error("ErrorCopyingDataBase");
            }
        }
    }

    private void copyDBFile() throws IOException {
        InputStream mInput = mContext.getAssets().open(DATABASE_NAME);
        //InputStream mInput = mContext.getResources().openRawResource(R.raw.info);
        OutputStream mOutput = new FileOutputStream(DB_PATH + DATABASE_NAME);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer)) > 0)
            mOutput.write(mBuffer, 0, mLength);
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }




 public void openDataBase() {
       String myPath = mContext.getDatabasePath(DATABASE_NAME).getPath();;

     if ( mDataBase != null && mDataBase.isOpen())
     {
         return;
     }
        mDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
    }

    public  void closeDatabase() {
        if (mDataBase !=null && mDataBase.isOpen());
        {
            mDataBase.close();
        }
    }



    public Project getProjectFromDatabase(String projectKey) {

        Project project = new Project();
        openDataBase();

        String query = (" SELECT * FROM Projects WHERE projectKey = '"+ projectKey +"'");
            Cursor c = mDataBase.rawQuery(query, null);
            try {

                c.moveToFirst();

                Log.println(Log.INFO, "projectKey",
                        c.getString(c.getColumnIndex("projectKey")));

                while (!c.isAfterLast()) {
                    if (c.getString(c.getColumnIndex("projectKey")) != null) {
                        project.setprojectKey(c.getString(c.getColumnIndex("projectKey")));
                    }
                    if (c.getString(c.getColumnIndex("fabricType")) != null) {
                        project.setfabricType(c.getString(c.getColumnIndex("fabricType")));
                    }
                    if (c.getString(c.getColumnIndex("ID")) != null) {
                        project.setId(c.getInt(c.getColumnIndex("ID")));
                    }
                    if (c.getString(c.getColumnIndex("project")) != null) {
                        project.setProject(c.getString(c.getColumnIndex("project")));
                    }

                    c.moveToNext();
                }
                c.close();
                closeDatabase();
               

            } catch (Exception e) {
                e.printStackTrace();
            }
        return project;
        }





    public List<Linksgirls> getLinksFromDatabase(String projectKey) {

        List<Linksgirls> myLinkList = new ArrayList<Linksgirls>();

        SQLiteDatabase mDatabase = getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_LINKS_NAME + " WHERE projectKey = '" + projectKey + "' "  + " AND boy = '1' ";



        Cursor c = mDatabase.rawQuery(query, null);
        try {

            c.moveToFirst();

//              Log.println(Log.INFO, "projectKey",
  //                    c.getString(c.getColumnIndex("projectKey")));

            while (!c.isAfterLast()) {
                Linksgirls link = new Linksgirls();
                if (c.getString(c.getColumnIndex("projectKey")) != null) {
                    link.setprojectKey(c.getString(c.getColumnIndex("projectKey")));

                }
                if (c.getString(c.getColumnIndex("naam")) != null) {
                    link.setname(c.getString(c.getColumnIndex("naam")));

                }
                if (c.getString(c.getColumnIndex("ID")) != null) {
                    link.setId(c.getInt(c.getColumnIndex("ID")));
                }
                if (c.getString(c.getColumnIndex("type")) != null) {
                    link.settype(c.getString(c.getColumnIndex("type")));
                }

                if (c.getString(c.getColumnIndex("link")) != null) {
                    link.setlink(c.getString(c.getColumnIndex("link")));
                }
                if (c.getString(c.getColumnIndex("image")) != null) {
                    link.setimage(c.getString(c.getColumnIndex("image")));
                }
                if (c.getString(c.getColumnIndex("boy")) != null) {
                    link.setboy(c.getInt(c.getColumnIndex("boy")));
                }
                if (c.getString(c.getColumnIndex("girl")) != null) {
                    link.setgirl(c.getInt(c.getColumnIndex("girl")));
                }

                myLinkList.add(link);
                c.moveToNext();
            }
            //   mDatabase.close();
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if(c != null){
                c.close();
            }

        }

        //myLinkList.get(0).getname()
            return myLinkList;

    }


    public List<Linksgirls> getgirlFromDatabase(String projectKey) {

        List<Linksgirls> myLinkListgirl = new ArrayList<Linksgirls>();

        SQLiteDatabase mDatabase = getReadableDatabase();

        String querygirl = "SELECT * FROM " + TABLE_LINKS_NAME + " WHERE projectKey ='" + projectKey + "' " +"  AND girl = '1' ";



        Cursor c = mDatabase.rawQuery(querygirl, null);
        try {

            c.moveToFirst();

//              Log.println(Log.INFO, "projectKey",
            //                    c.getString(c.getColumnIndex("projectKey")));

            while (!c.isAfterLast()) {
                Linksgirls link = new Linksgirls();
                if (c.getString(c.getColumnIndex("projectKey")) != null) {
                    link.setprojectKey(c.getString(c.getColumnIndex("projectKey")));

                }
                if (c.getString(c.getColumnIndex("naam")) != null) {
                    link.setname(c.getString(c.getColumnIndex("naam")));

                }
                if (c.getString(c.getColumnIndex("ID")) != null) {
                    link.setId(c.getInt(c.getColumnIndex("ID")));
                }
                if (c.getString(c.getColumnIndex("type")) != null) {
                    link.settype(c.getString(c.getColumnIndex("type")));
                }

                if (c.getString(c.getColumnIndex("link")) != null) {
                    link.setlink(c.getString(c.getColumnIndex("link")));
                }
                if (c.getString(c.getColumnIndex("image")) != null) {
                    link.setimage(c.getString(c.getColumnIndex("image")));
                }
                if (c.getString(c.getColumnIndex("boy")) != null) {
                    link.setboy(c.getInt(c.getColumnIndex("boy")));
                }
                if (c.getString(c.getColumnIndex("girl")) != null){
                    link.setgirl(c.getInt(c.getColumnIndex("girl")));
                }

                myLinkListgirl.add(link);
                c.moveToNext();
            }
            //   mDatabase.close();
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if(c != null){
                c.close();
            }

        }

        //myLinkList.get(0).getname()
        return myLinkListgirl;

    }








}









