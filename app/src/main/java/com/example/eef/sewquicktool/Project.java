package com.example.eef.sewquicktool;

public class Project {


    private static final String DATABASE_NAME = "girl_Database";
    public static final String TABLE_NAME= "Projects";
    public static final String COLUMN_ID = "ID";
    public static final String COLUMN_PROJECT = "project";
    public static final String COLUMN_FABRICTYPE = "fabricType";
    public static final String COLUMN_PROJECTKEY = "projectKey";

    private int id;
    private String project;
    private String fabricType;
    private String projectKey;


    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_PROJECT + " TEXT,"
                    + COLUMN_FABRICTYPE + " TEXT"
                    + COLUMN_PROJECTKEY + " TEXT"
                    + ")";


    public Project () {

    }

    public Project(int id, String project, String fabricType, String projectKey) {
        this.id = id;
        this.project = project;
        this.fabricType = fabricType;
        this.projectKey = projectKey;
    }

    public int getId() {
        return id;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getfabricType() {
        return fabricType;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setfabricType(String fabricType) {
        this.fabricType = fabricType;
    }

    public String getprojectKey() {
        return projectKey;
    }

    public void setprojectKey(String projectKey) {
        this.projectKey = projectKey;
    }


}



