package com.example.eef.sewquicktool;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.example.eef.sewquicktool.R;


import java.util.ArrayList;
import java.util.List;



public class bottom_fragment extends Fragment implements View.OnClickListener {



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.bottom_fragment, container, false);


//machine tutorials
       ImageView machineView0 = (ImageView) v.findViewById(R.id.imageM0);
       machineView0.setOnClickListener(this);
       ImageView machineView1 = (ImageView) v.findViewById(R.id.imageM1);
       machineView1.setOnClickListener(this);
       ImageView machineView2 = (ImageView) v.findViewById(R.id.imageM2);
       machineView2.setOnClickListener(this);

       //needles tutorials
       ImageView needleView0 = (ImageView) v.findViewById(R.id.imageN0);
       needleView0.setOnClickListener(this);
       ImageView needleView1 = (ImageView) v.findViewById(R.id.imageN1);
       needleView1.setOnClickListener(this);
       ImageView needleView2 = (ImageView) v.findViewById(R.id.imageN2);
       needleView2.setOnClickListener(this);
       ImageView needleView3 = (ImageView) v.findViewById(R.id.imageN3);
       needleView3.setOnClickListener(this);

       //Stitches tutorials
        ImageView stitchView0 = (ImageView) v.findViewById(R.id.imageST0);
        stitchView0.setOnClickListener(this);
        ImageView stitchView1 = (ImageView) v.findViewById(R.id.imageST1);
        stitchView1.setOnClickListener(this);
        ImageView stitchView2 = (ImageView) v.findViewById(R.id.imageST2);
        stitchView2.setOnClickListener(this);
        ImageView stitchView3 = (ImageView) v.findViewById(R.id.imageST3);
        stitchView3.setOnClickListener(this);

        //troubleshooting tutorials
        ImageView trouble0 = (ImageView) v.findViewById(R.id.imageTR0);
        trouble0.setOnClickListener(this);
        ImageView trouble1 = (ImageView) v.findViewById(R.id.imageTR1);
        trouble1.setOnClickListener(this);
        ImageView trouble2 = (ImageView) v.findViewById(R.id.imageTR2);
        trouble2.setOnClickListener(this);
        ImageView trouble3 = (ImageView) v.findViewById(R.id.imageTR3);
        trouble3.setOnClickListener(this);

        //bias en zipper tutorials
        ImageView zip0 = (ImageView) v.findViewById(R.id.imageZIP0);
        zip0.setOnClickListener(this);
        ImageView zip1 = (ImageView) v.findViewById(R.id.imageZIP1);
        zip1.setOnClickListener(this);
        ImageView zip2 = (ImageView) v.findViewById(R.id.imageZIP2);
        zip2.setOnClickListener(this);
        ImageView zip3 = (ImageView) v.findViewById(R.id.imageZIP3);
        zip3.setOnClickListener(this);




        String url = "https://img.youtube.com/vi/MRE4TABLswU/0.jpg";
        String url1 = "https://img.youtube.com/vi/IGITrkYdjJs/0.jpg";
        String url2 = "https://img.youtube.com/vi/PMTzXsyOVoI/0.jpg";

        String url3 = "http://3.bp.blogspot.com/-FXUt0cfKGUA/U9ZpfSgO_EI/AAAAAAAAJao/DOT9FKYKBao/s1600/Color_Code_Chart_Home_Page.jpg";
        String url4 = "https://i2.wp.com/blog.treasurie.com/wp-content/uploads/2016/08/sewing-machine-needles-size-guide-5.jpg?w=1200&ssl=1";
        String url5 = "https://www.lovesewingmag.co.uk/wp-content/uploads/media/k2/items/src/b689fad0280b286c898256c8d3b6ee9e.jpg";
        String url6 = "http://www.simplymodernmom.com/wp-content/uploads/2010/05/IMG_8878.jpg";

        String url7 = "https://mellysews.com/wp-content/uploads/2017/02/KnitStitchesPin.jpg";
        String url8 = "https://crazylittleprojects.com/wp-content/uploads/2017/06/How-to-Sew-a-ZigZag-Stitch.png";
        String url9 = "https://d2droglu4qf8st.cloudfront.net/2017/08/345160/What-Types-of-Thread-to-Use-Infographic-whitespace_Large500_ID-2405443.png";
        String url10 = "https://img.youtube.com/vi/xaSGtm_gYrc/0.jpg";

        String url11 = "https://i2.wp.com/www.awilson.co.uk/wp-content/uploads/2016/03/sewing-machine-pinterest.jpg";
        String url12 = "https://www.craftaholicsanonymous.net/wp-content/uploads/2015/02/Bobbin-Help.jpg";
        String url13 = "https://i1.wp.com/sewsweetness.com/wp-content/uploads/2011/08/6047254023_3b45674275.jpg";
        String url14 = "https://img.youtube.com/vi/VIqSb9MD5q4/0.jpg";

        String url15 = "http://smashedpeasandcarrots.com/wp-content/uploads/2011/01/IMG_7613.jpg";
        String url16 = "https://makeit-loveit.com/wp-content/uploads/2015/05/How-to-make-one-CONTINUOUS-strip-of-BIAS-TAPE-1.jpg";
        String url17 = "https://img.youtube.com/vi/oHDDhUzIXPY/0.jpg";
        String url18 = "https://c2.staticflickr.com/9/8338/29343336785_0c9f745c48_z.jpg";


        GlideApp.with(this)
                .load(url)
                .fitCenter()
                .into(machineView0);


        GlideApp.with(this)
                .load(url1)
                .fitCenter()
                .into(machineView1);

        GlideApp.with(this)
                .load(url2)
                .fitCenter()
                .into(machineView2);


        GlideApp.with(this)
                .load(url3)
                .fitCenter()
                .into(needleView0);

        GlideApp.with(this)
                .load(url4)
                .fitCenter()
                .into(needleView1);

        GlideApp.with(this)
                .load(url5)
                .fitCenter()
                .into(needleView2);

        GlideApp.with(this)
                .load(url6)
                .fitCenter()
                .into(needleView3);


        GlideApp.with(this)
                .load(url7)
                .fitCenter()
                .into(stitchView0);

        GlideApp.with(this)
                .load(url8)
                .fitCenter()
                .into(stitchView1);

        GlideApp.with(this)
                .load(url9)
                .fitCenter()
                .into(stitchView2);

        GlideApp.with(this)
                .load(url10)
                .fitCenter()
                .into(stitchView3);


        GlideApp.with(this)
                .load(url11)
                .fitCenter()
                .into(trouble0);

        GlideApp.with(this)
                .load(url12)
                .fitCenter()
                .into(trouble1);

        GlideApp.with(this)
                .load(url13)
                .fitCenter()
                .into(trouble2);

        GlideApp.with(this)
                .load(url14)
                .fitCenter()
                .into(trouble3);

        GlideApp.with(this)
                .load(url15)
                .fitCenter()
                .into(zip0);

        GlideApp.with(this)
                .load(url16)
                .fitCenter()
                .into(zip1);

        GlideApp.with(this)
                .load(url17)
                .fitCenter()
                .into(zip2);

        GlideApp.with(this)
                .load(url18)
                .fitCenter()
                .into(zip3);



        return v;

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.imageM0:
                Intent M0Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=MRE4TABLswU"));
                startActivity(M0Intent);

                break;

            case R.id.imageM1:
             Intent M1Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=IGITrkYdjJs"));
             startActivity(M1Intent);


                break;

            case R.id.imageM2:
                Intent M2Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=PMTzXsyOVoI"));
                startActivity(M2Intent);

                break;

            case R.id.imageN0:
                Intent N0Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://createkidscouture.blogspot.com/2014/07/all-about-needles.html"));
                startActivity(N0Intent);

                break;

            case R.id.imageN1:
                Intent N1Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.wawak.com/assets/images/infographics/Needle_Types.jpg"));
                startActivity(N1Intent);


                break;

            case R.id.imageN2:
                Intent N2Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.lovesewingmag.co.uk/learn-to-sew/machine-sewing/item/501-sewing-machine-presser-foot-guide/"));
                startActivity(N2Intent);

                break;

            case R.id.imageN3:
                Intent N3Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.simplymodernmom.com/2010/05/sewing-101-types-of-presser-foot/"));
                startActivity(N3Intent);

                break;

            case R.id.imageST0:
                Intent ST0Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://crazylittleprojects.com/learn-to-sew-series-lesson-2-zigzag-stitch-2/"));
                startActivity(ST0Intent);

                break;

            case R.id.imageST1:
                Intent ST1Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://mellysews.com/2017/02/types-stretch-stitches-sewing-knits.html"));
                startActivity(ST1Intent);

                break;

            case R.id.imageST2:
                Intent ST2Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.allfreesewing.com/Sewing-Tips-and-Tricks/What-Types-of-Thread-to-Use-A-Guide/"));
                startActivity(ST2Intent);

                break;

            case R.id.imageST3:
                Intent ST3Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=xaSGtm_gYrc"));
                startActivity(ST3Intent);

                break;



            case R.id.imageTR0:
                Intent TR0Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.awilson.co.uk/fix-jammed-sewing-machine/"));
                startActivity(TR0Intent);

                break;

            case R.id.imageTR1:
                Intent TR1Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.craftaholicsanonymous.net/how-to-fix-bobbin-tension"));
                startActivity(TR1Intent);

                break;

            case R.id.imageTR2:
                Intent TR2Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://sewsweetness.com/2011/08/sewing-back-to-school-stitches-tension.html"));
                startActivity(TR2Intent);

                break;

            case R.id.imageTR3:
                Intent TR3Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=VIqSb9MD5q4"));
                startActivity(TR3Intent);


                break;

            case R.id.imageZIP0:
                Intent ZIP0Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://smashedpeasandcarrots.com/how-to-sew-bias-tape-a-tutorial/"));
                startActivity(ZIP0Intent);

                break;

            case R.id.imageZIP1:
                Intent ZIPIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://makeit-loveit.com/how-to-make-one-continuous-strip-of-bias-tape-from-one-square-of-fabric"));
                startActivity(ZIPIntent);

                break;

            case R.id.imageZIP2:
                Intent ZIP2Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=oHDDhUzIXPY"));
                startActivity(ZIP2Intent);

                break;

            case R.id.imageZIP3:
                Intent ZIP3Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.made-by-rae.com/2016/09/bias-binding-tutorial-french-method/"));
                startActivity(ZIP3Intent);

                break;


            default:
                break;
        }

    }

    }

